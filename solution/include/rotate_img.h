#ifndef ROTATE_IMG_H_INCLUDED
#define ROTATE_IMG_H_INCLUDED
#include "bmp_form.h"
#include "image.h"

struct img rotate_img(const struct img img);

void update_header(struct bmp_header* header,struct img img);

#endif
