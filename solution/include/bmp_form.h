#ifndef BMP_FORM_H_INCLUDED
#define BMP_FORM_H_INCLUDED
#include "image.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#define BMP_SIGNATURE 19778

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};

enum read_status
{
        READ_SUCCESS = 0,
        READ_INVALID_BYTES,
        READ_INVALID_HEADER,
        FILE_NOT_BMP
};

enum write_status
{
        WRITE_SUCCESS = 0,
        WRITE_INVALID_BYTES,
        WRITE_INVALID_HEADER
};

void bmp_print(struct bmp_header header);

enum read_status from_bmp(FILE* file,struct bmp_header* header,struct img* img);

enum write_status to_bmp (FILE* file,const struct img img,const struct bmp_header header);

#endif
