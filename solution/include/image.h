#include <stdint.h>
#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED


struct __attribute__((packed)) pixel{
    uint8_t b, g, r;
};

struct img{
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

void free_img(struct img img);

void* create_image(uint64_t width, uint64_t height);

#endif
