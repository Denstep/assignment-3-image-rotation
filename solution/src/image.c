#include "image.h"
#include <stdint.h>
#include <stdlib.h>

void free_img(struct img img){
    free(img.data);
}

void* create_image(uint64_t width, uint64_t height){
    return malloc(sizeof(struct pixel)*width*height);
}
