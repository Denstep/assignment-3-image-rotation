#include "bmp_form.h"
#include "stdbool.h"
#include <stdlib.h>

void bmp_print(struct bmp_header header){
    printf("bfType: %" PRIu16 "\n", header.bfType);
    printf("bfileSize %" PRIu32 "\n", header.bfileSize);
    printf("bfReserved %" PRIu32 "\n", header.bfReserved);
    printf("bOffBits %" PRIu32 "\n", header.bOffBits);
    printf("biSize %" PRIu32 "\n", header.biSize);
    printf("biWidth %" PRIu32 "\n", header.biWidth);
    printf("biHeight %" PRIu32 "\n", header.biHeight);
    printf("biPlanes %" PRIu16 "\n", header.biPlanes);
    printf("biBitCount %" PRIu16 "\n", header.biBitCount);
    printf("biCompression %" PRIu32 "\n", header.biCompression);
    printf("biSizeImage %" PRIu32 "\n", header.biSizeImage);
    printf("biXPelsPerMeter %" PRIu32 "\n", header.biXPelsPerMeter);
    printf("biYPelsPerMeter %" PRIu32 "\n", header.biYPelsPerMeter);
    printf("biClrUsed %" PRIu32 "\n", header.biClrUsed);
    printf("biClrImportant %" PRIu32 "\n", header.biClrImportant);
}

static bool read_bmp_header(FILE* file, struct bmp_header* header){
        return fread(header, sizeof(struct bmp_header), 1, file)==1;
}

static bool write_bmp_header(FILE* file, struct bmp_header header){
        return fwrite(&header, sizeof(struct bmp_header), 1, file)==1;
}

static uint8_t padding_b(struct bmp_header header){
    return (4-(header.biWidth*3)%4)%4;
}

static bool read_pixels(FILE* file, struct bmp_header header, struct img* img) {

    if(fseek(file, header.bOffBits, SEEK_SET)!=0){
        return false;
    }
    uint8_t padding_bytes = padding_b(header);
    img->width=header.biWidth;
    img->height=header.biHeight;
    img->data=create_image(header.biWidth,header.biHeight);
    for(uint32_t i=0; i<header.biHeight; i++){
        if(fread(img->data+i*header.biWidth, sizeof(struct pixel), header.biWidth, file)!=header.biWidth){
            return false;
        }
        if(fseek(file, padding_bytes, SEEK_CUR)!=0){
        return false;
        }

    }
    return true;
}

static bool write_pixels(FILE* file,struct img img, struct bmp_header header){
    if(fseek(file, header.bOffBits, SEEK_SET)!=0){
        return false;
        }

    uint8_t padding_bytes = padding_b(header);

    for(uint32_t i=0; i<header.biHeight; i++){
        if(fwrite(img.data+i*header.biWidth, sizeof(struct pixel), header.biWidth, file)!=header.biWidth){
         return false;
        }
        if(fseek(file, padding_bytes, SEEK_CUR)!=0){
        return false;
        }

    }
    return true;
}

enum read_status from_bmp(FILE* file, struct bmp_header* header, struct img* img){
    if(!read_bmp_header(file, header))
        return READ_INVALID_HEADER;

    if (header == NULL) {
        return READ_INVALID_HEADER;
    }

    if(header->bfType!=BMP_SIGNATURE){
        return FILE_NOT_BMP;
    }

    bmp_print(*header);

    if(!read_pixels(file, *header, img))
        return READ_INVALID_BYTES;

    return READ_SUCCESS;

}

enum write_status to_bmp(FILE* file,const struct img img,const struct bmp_header header) {
    if(!write_bmp_header(file, header))
        return WRITE_INVALID_HEADER;
    if(!write_pixels(file, img, header))
        return WRITE_INVALID_BYTES;

    return WRITE_SUCCESS;
}
