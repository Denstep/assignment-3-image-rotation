#include "bmp_form.h"
#include "image.h"
#include "rotate_img.h"
#include <stdlib.h>

struct img rotate_img(const struct img img){
	uint64_t width=img.height;
	uint64_t height=img.width;

	struct pixel* pixels = create_image(width,height);
	 for(uint64_t i=0; i<height; i++){
	 	for(uint64_t j=0; j<width; j++){
        	pixels[i*width+j]=img.data[(img.height-j-1)*img.width+i];
     	}
	 }

	return (struct img){
		.width=width,
		.height=height,
		.data=pixels
	};
}

void update_header(struct bmp_header* header,const struct img img){
	header->biHeight=img.height;
	header->biWidth=img.width;
}
