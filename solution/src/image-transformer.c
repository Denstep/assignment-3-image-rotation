#include "bmp_form.h"
#include "image.h"
#include "rotate_img.h"
#include <stdint.h>
#include <stdio.h>

#define ARGUMENTS_ERROR 6
#define FILE_NOT_FOUND 7

int main(int argc, char** argv){

     if (argc != 3)
     {
         printf("Incorrect number of arguments");
         return ARGUMENTS_ERROR;
     }


    FILE* file_input = fopen(argv[1], "rb");
    if(file_input==NULL){
        printf("File not found\n");
        return FILE_NOT_FOUND;
    }

    FILE* file_output = fopen(argv[2], "wb");
    if(file_output==NULL){
        printf("File not found\n");
        return FILE_NOT_FOUND;
    }

    struct img img_in={0};
    struct img img_out={0};

    struct bmp_header header_in={0};

    switch (from_bmp(file_input, &header_in, &img_in)){
        case READ_SUCCESS:
        {
            printf("File read corect!\n");
            break;
        }
        case READ_INVALID_HEADER:
        {
            printf("Read header error!\n");
            return READ_INVALID_HEADER;
        }
        case READ_INVALID_BYTES:
        {
            printf("Read pixels error!\n");
            return READ_INVALID_BYTES;
        }
        case FILE_NOT_BMP:
        {
            printf("File is not bmp!\n");
            return FILE_NOT_BMP;
        }
        default:
            break;
    }

    img_out = rotate_img(img_in);
    update_header(&header_in, img_out);

    switch (to_bmp(file_output, img_out, header_in)){
        case WRITE_SUCCESS:
        {
            printf("File write corect!\n");
            break;
        }
        case WRITE_INVALID_BYTES:
        {
            printf("Write pixels error!\n");
            break;
        }
        case WRITE_INVALID_HEADER:
        {
            printf("Write header error!\n");
            break;
        }
    }
    fclose(file_output);
    fclose(file_input);
    free_img(img_in);
    free_img(img_out);
    return 0;
}
